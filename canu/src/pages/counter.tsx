import { useEffect, useState } from "react";

const Social = () => {
    const [subscribers, setSubscribers] = useState(0);
    useEffect(() => {
        const interval = setInterval(() => {
            setSubscribers(subs => subs + 1);
        }, 1000);
        return () => clearInterval(interval);
    }, []);
    return (
        <>
            <head>
                <title>Counter</title>
            </head>
            <body>
                <h1>{subscribers} seconds that I have wasted</h1>
                <p>
                Hi bro!
                </p>
            </body>
        </>
    );
}

export default Social;