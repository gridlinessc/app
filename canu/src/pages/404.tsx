const ResourceNotFound = () => {
    return (
        <h1>This resource doesn't exist!</h1>
    )
}

export default ResourceNotFound;