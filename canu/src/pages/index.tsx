import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

import Navbar from '../components/Navbar'

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
        <Head>
            <html lang="en"></html>
            <link rel="icon" type="image/x-icon" href="/favicon.ico" />
            <title>Create Next App</title>
        </Head>
        <Image src="/favicon.ico" alt="me" width="64" height="64" />
        <Navbar /> 
    </div>
  )
}

export default Home
